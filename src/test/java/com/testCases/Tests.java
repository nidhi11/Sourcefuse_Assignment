package com.testCases;

import static org.testng.Assert.assertEquals;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class Tests {
	WebDriverWait wait;
	WebDriver driver;	
	WebElement firstname;
	WebElement lastName;
	By firstNameLocator = By.xpath("//*[@id=\"fnameInput\"]/input");
	By lastNameLocator = By.xpath("//div[@id=\"lnameInput\"]/input");
	By emailLocator = By.xpath("//div[@id=\"emailInput\"]/input");
	By currentCompanyLocator = By.xpath("//div[@id=\"curCompanyInput\"]/input");
	By dobLocator = By.xpath("//div[@id=\"DOBInput\"]/div/input");
	By mobileLocator = By.xpath("//div[@id=\"mobInput\"]/input");
	By postionLocator = By.xpath("//div[@id=\"positionInput\"]/input");
	By portfolioLocator = By.xpath("//div[@id=\"portfolioInput\"]/input");
	By salaryLocator = By.xpath("//div[@id=\"salaryInput\"]/input");
	By resumeLocator = By.xpath("//div[@id=\"resumeInput\"]/input");
	By radiobtnLocator = By.xpath("//div/input[@id=\"yes\"]");
	By submitbtnLocator = By.xpath("//button[@type=\"submit\"]");
	By firstNameLabelLocator = By.xpath("//label[@for=\"fname\"]");
	By lastNameLabelLocator = By.xpath("//label[@for=\"lname\"]");
	By emailLabelLocator = By.xpath("//label[@for=\"email\"]");
	By companyLabelLocator = By.xpath("//label[@for=\"curCompany\"]");
	By mobileLabelLocator = By.xpath("//label[@for=\"mob\"]");
	By dobLabelLocator = By.xpath("//label[@for=\"DOB\"]");
	By positionLabelLocator = By.xpath("//label[@for=\"position\"]");
	By portfolioLabelLocator = By.xpath("//label[@for=\"portfolio\"]");
	By salaryLabelLocator = By.xpath("//label[@for=\"salary\"]");
	By resumeLabelLocator = By.xpath("//label[@for=\"resume\"]");
	By radioLabelLocator = By.xpath("//label[@for=\"relocate\"]");
	String textInsideInputBox;
	String textInsideInputLabelBox;
	List<String> expectedlist = new ArrayList<String>();
	List<String> actuallist = new ArrayList<String>();
	Boolean a;

	@BeforeClass
	public void initialSetup() {
		System.setProperty("webdriver.chrome.driver", "/Users/nidhichauhan/Downloads/chromedriver");

	}

	@BeforeMethod

	public void Methodsetup() {
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, 30, 5000);
		driver.get("http://sfwebhtml:t63KUfxL5vUyFLG4eqZNUcuRQ@sfwebhtml.sourcefuse.com/automation-form-demo-for-interview/");
		driver.manage().window().fullscreen();	

	}


	@Test(testName = "Verify Hard Assertion", description = "Verifies Required fields with hard assertion", priority = 3)
	public void HardAssertionTest() {

		firstname = wait.until(ExpectedConditions.visibilityOfElementLocated(firstNameLocator));
		firstname.sendKeys("Nidhi");
		textInsideInputBox = firstname.getAttribute("value");
		Assert.assertEquals(textInsideInputBox, "Nidhi");

		lastName = wait.until(ExpectedConditions.visibilityOfElementLocated(lastNameLocator));
		lastName.sendKeys("Chauhan");
		textInsideInputBox = lastName.getAttribute("value");
		Assert.assertEquals(textInsideInputBox, "Chauhan");


		WebElement email = wait.until(ExpectedConditions.visibilityOfElementLocated(emailLocator));
		email.sendKeys("nidhichauhan8898@gmail.com");
		textInsideInputBox = email.getAttribute("value");
		Assert.assertEquals(textInsideInputBox, "nidhichauhan8898@gmail.com");


		WebElement company = wait.until(ExpectedConditions.visibilityOfElementLocated(currentCompanyLocator));
		company.sendKeys("IBM");
		textInsideInputBox = company.getAttribute("value");
		Assert.assertEquals(textInsideInputBox, "IBM");

		WebElement mobile = wait.until(ExpectedConditions.visibilityOfElementLocated(mobileLocator));
		mobile.sendKeys("8851273955");
		textInsideInputBox = mobile.getAttribute("value");
		Assert.assertEquals(textInsideInputBox, "8851273955");

		WebElement dob = wait.until(ExpectedConditions.visibilityOfElementLocated(dobLocator));
		dob.sendKeys("11/09/1996");
		textInsideInputBox = dob.getAttribute("value");
		Assert.assertEquals(textInsideInputBox, "11/09/1996");

		WebElement position = wait.until(ExpectedConditions.visibilityOfElementLocated(postionLocator));
		position.sendKeys("Test Engineer");
		textInsideInputBox = position.getAttribute("value");
		Assert.assertEquals(textInsideInputBox, "Test Engineer");

		WebElement portfolio = wait.until(ExpectedConditions.visibilityOfElementLocated(portfolioLocator));
		portfolio.sendKeys("www.linkedin.com/in/nidhi-chauhan-3a26b813b/");
		textInsideInputBox = portfolio.getAttribute("value");
		Assert.assertEquals(textInsideInputBox, "http://www.linkedin.com/in/nidhi-chauhan-3a26b813b/");

		WebElement salary = wait.until(ExpectedConditions.visibilityOfElementLocated(salaryLocator));
		salary.sendKeys(" 8 LPA");
		textInsideInputBox = salary.getAttribute("value");
		Assert.assertEquals(textInsideInputBox, " 8 LPA");

		WebElement resume = wait.until(ExpectedConditions.visibilityOfElementLocated(resumeLocator));
		resume.sendKeys("/Users/nidhichauhan/eclipse-workspace/Sourcefuse_Assignment/Data/51183.xlsx");
		textInsideInputBox = resume.getAttribute("value");
		Assert.assertEquals(textInsideInputBox, "C:\\fakepath\\51183.xlsx");

		WebElement radiobtn = wait.until(ExpectedConditions.visibilityOfElementLocated(radiobtnLocator));
		radiobtn.click();
		Assert.assertEquals(true, true);

		WebElement submitbtn = wait.until(ExpectedConditions.visibilityOfElementLocated(submitbtnLocator));
		submitbtn.click();
		Assert.assertEquals(true, true);

		driver.close();
	}


	@Test(testName = "Verify Soft Assertion", description = "Verifies Required fields with soft assertion", priority = 2)

	public void softAssertionTest() {

		SoftAssert sa= new SoftAssert();
		WebElement firstname = wait.until(ExpectedConditions.visibilityOfElementLocated(firstNameLocator));
		firstname.sendKeys("Nidhi");
		textInsideInputBox = firstname.getAttribute("value");
		sa.assertEquals(textInsideInputBox,"Nidhi" );

		WebElement lastName = wait.until(ExpectedConditions.visibilityOfElementLocated(lastNameLocator));
		lastName.sendKeys("Chauhan");
		textInsideInputBox = lastName.getAttribute("value");
		sa.assertEquals(textInsideInputBox, "Chauhan");


		WebElement email = wait.until(ExpectedConditions.visibilityOfElementLocated(emailLocator));
		email.sendKeys("nidhichauhan8898@gmail.com");
		textInsideInputBox = email.getAttribute("value");
		sa.assertEquals(textInsideInputBox, "nidhichauhan8898@gmail.com");

		WebElement company = wait.until(ExpectedConditions.visibilityOfElementLocated(currentCompanyLocator));
		company.sendKeys("IBM");
		textInsideInputBox = company.getAttribute("value");
		sa.assertEquals(textInsideInputBox, "IBM");

		WebElement mobile = wait.until(ExpectedConditions.visibilityOfElementLocated(mobileLocator));
		mobile.sendKeys("8851273955");
		textInsideInputBox = mobile.getAttribute("value");
		sa.assertEquals(textInsideInputBox, "8851273955");

		WebElement dob = wait.until(ExpectedConditions.visibilityOfElementLocated(dobLocator));
		dob.sendKeys("11/09/1996");
		textInsideInputBox = dob.getAttribute("value");
		sa.assertEquals(textInsideInputBox, "11/09/1996");

		WebElement position = wait.until(ExpectedConditions.visibilityOfElementLocated(postionLocator));
		position.sendKeys("Test Engineer");
		textInsideInputBox = position.getAttribute("value");
		sa.assertEquals(textInsideInputBox, "Test Engineer");

		WebElement portfolio = wait.until(ExpectedConditions.visibilityOfElementLocated(portfolioLocator));
		portfolio.sendKeys("www.linkedin.com/in/nidhi-chauhan-3a26b813b/");
		textInsideInputBox = portfolio.getAttribute("value");
		sa.assertEquals(textInsideInputBox, "http://www.linkedin.com/in/nidhi-chauhan-3a26b813b/");

		WebElement salary = wait.until(ExpectedConditions.visibilityOfElementLocated(salaryLocator));
		salary.sendKeys(" 8 LPA");
		textInsideInputBox = salary.getAttribute("value");
		sa.assertEquals(textInsideInputBox, " 8 LPA");

		WebElement resume = wait.until(ExpectedConditions.visibilityOfElementLocated(resumeLocator));
		resume.sendKeys("/Users/nidhichauhan/eclipse-workspace/Sourcefuse_Assignment/Data/51183.xlsx");

		
		textInsideInputBox = resume.getAttribute("value");
		sa.assertEquals(textInsideInputBox, "C:\\fakepath\\51183.xlsx");


		WebElement radiobtn = wait.until(ExpectedConditions.visibilityOfElementLocated(radiobtnLocator));
		radiobtn.click();
		sa.assertEquals(true, true);

		WebElement submitbtn = wait.until(ExpectedConditions.visibilityOfElementLocated(submitbtnLocator));
		submitbtn.click();
		sa.assertEquals(true, true);

		sa.assertAll();
		driver.close();
	}



	@Test(testName = "Verify form submission", description = " Fill required fields and submit form ", priority = 4)
	public void SubmitFormTest() {

		WebElement firstname = wait.until(ExpectedConditions.visibilityOfElementLocated(firstNameLocator));
		firstname.sendKeys("Nidhi");

		WebElement lastName = wait.until(ExpectedConditions.visibilityOfElementLocated(lastNameLocator));
		lastName.sendKeys("Chauhan");


		WebElement email = wait.until(ExpectedConditions.visibilityOfElementLocated(emailLocator));
		email.sendKeys("nidhichauhan8898@gmail.com");


		WebElement company = wait.until(ExpectedConditions.visibilityOfElementLocated(currentCompanyLocator));
		company.sendKeys("IBM");

		WebElement mobile = wait.until(ExpectedConditions.visibilityOfElementLocated(mobileLocator));
		mobile.sendKeys("8851273955");

		WebElement dob = wait.until(ExpectedConditions.visibilityOfElementLocated(dobLocator));
		dob.sendKeys("11/09/1996");

		WebElement position = wait.until(ExpectedConditions.visibilityOfElementLocated(postionLocator));
		position.sendKeys("Test Engineer");

		WebElement portfolio = wait.until(ExpectedConditions.visibilityOfElementLocated(portfolioLocator));
		portfolio.sendKeys("www.linkedin.com/in/nidhi-chauhan-3a26b813b/");

		WebElement salary = wait.until(ExpectedConditions.visibilityOfElementLocated(salaryLocator));
		salary.sendKeys(" 8 LPA");

		WebElement resume = wait.until(ExpectedConditions.visibilityOfElementLocated(resumeLocator));
		resume.sendKeys("/Users/nidhichauhan/eclipse-workspace/Sourcefuse_Assignment/Data/51183.xlsx");


		WebElement radiobtn = wait.until(ExpectedConditions.visibilityOfElementLocated(radiobtnLocator));
		radiobtn.click();


		WebElement submitbtn = wait.until(ExpectedConditions.visibilityOfElementLocated(submitbtnLocator));
		submitbtn.click();
		Assert.assertEquals(true, true);

		driver.close();
	}

	@Test(testName = "Verify Required fields", description = "Verifies required fields on console by clicking submit button", priority = 1)

	public void softAssesrtionwithoutfillingTest() {

		WebElement submitbtn = wait.until(ExpectedConditions.visibilityOfElementLocated(submitbtnLocator));
		submitbtn.click();
		firstname = wait.until(ExpectedConditions.visibilityOfElementLocated(firstNameLocator));

		textInsideInputBox = firstname.getAttribute("value");

		if (textInsideInputBox.isEmpty()) {
			WebElement firstNamelabel = wait.until(ExpectedConditions.visibilityOfElementLocated(firstNameLabelLocator));
			textInsideInputLabelBox = firstNamelabel.getText();
			System.out.println(textInsideInputLabelBox);

		}

		else {
			System.out.println(textInsideInputBox);
		}


		WebElement lastName = wait.until(ExpectedConditions.visibilityOfElementLocated(lastNameLocator));
		textInsideInputBox = lastName.getAttribute("value");



		if (textInsideInputBox.isEmpty()) {
			WebElement lastNamelabel = wait.until(ExpectedConditions.visibilityOfElementLocated(lastNameLabelLocator));
			textInsideInputLabelBox = lastNamelabel.getText();
			System.out.println(textInsideInputLabelBox);

		}

		else {
			System.out.println(textInsideInputBox);
		}


		WebElement email = wait.until(ExpectedConditions.visibilityOfElementLocated(emailLocator));
		textInsideInputBox = email.getAttribute("value");




		if (textInsideInputBox.isEmpty()) {
			WebElement emaillabel = wait.until(ExpectedConditions.visibilityOfElementLocated(emailLabelLocator));
			textInsideInputLabelBox = emaillabel.getText();
			System.out.println(textInsideInputLabelBox);

		}

		else {
			System.out.println(textInsideInputBox);
		}


		WebElement company = wait.until(ExpectedConditions.visibilityOfElementLocated(currentCompanyLocator));
		textInsideInputBox = company.getAttribute("value");

		if (textInsideInputBox.isEmpty()) {
			WebElement companylabel = wait.until(ExpectedConditions.visibilityOfElementLocated(companyLabelLocator));
			textInsideInputLabelBox = companylabel.getText();
			System.out.println(textInsideInputLabelBox);

		}

		else {
			System.out.println(textInsideInputBox);
		}


		WebElement mobile = wait.until(ExpectedConditions.visibilityOfElementLocated(mobileLocator));
		textInsideInputBox = mobile.getAttribute("value");

		if (textInsideInputBox.isEmpty()) {
			WebElement mobilelabel = wait.until(ExpectedConditions.visibilityOfElementLocated(mobileLabelLocator));
			textInsideInputLabelBox = mobilelabel.getText();
			System.out.println(textInsideInputLabelBox);

		}

		else {
			System.out.println(textInsideInputBox);
		}


		WebElement dob = wait.until(ExpectedConditions.visibilityOfElementLocated(dobLocator));
		textInsideInputBox = dob.getAttribute("value");

		if (textInsideInputBox.isEmpty()) {
			WebElement doblabel = wait.until(ExpectedConditions.visibilityOfElementLocated(dobLabelLocator));
			textInsideInputLabelBox = doblabel.getText();
			System.out.println(textInsideInputLabelBox);

		}

		else {
			System.out.println(textInsideInputBox);
		}


		WebElement position = wait.until(ExpectedConditions.visibilityOfElementLocated(postionLocator));
		textInsideInputBox = position.getAttribute("value");

		if (textInsideInputBox.isEmpty()) {
			WebElement positionlabel = wait.until(ExpectedConditions.visibilityOfElementLocated(positionLabelLocator));
			textInsideInputLabelBox = positionlabel.getText();
			System.out.println(textInsideInputLabelBox);

		}

		else {
			System.out.println(textInsideInputBox);
		}


		WebElement portfolio = wait.until(ExpectedConditions.visibilityOfElementLocated(portfolioLocator));
		textInsideInputBox = portfolio.getAttribute("value");

		if (textInsideInputBox.isEmpty()) {
			WebElement portfoliolabel = wait.until(ExpectedConditions.visibilityOfElementLocated(portfolioLabelLocator));
			textInsideInputLabelBox = portfoliolabel.getText();
			System.out.println(textInsideInputLabelBox);

		}

		else {
			System.out.println(textInsideInputBox);
		}


		WebElement salary = wait.until(ExpectedConditions.visibilityOfElementLocated(salaryLocator));
		textInsideInputBox = salary.getAttribute("value");

		if (textInsideInputBox.isEmpty()) {
			WebElement salarylabel = wait.until(ExpectedConditions.visibilityOfElementLocated(salaryLabelLocator));
			textInsideInputLabelBox = salarylabel.getText();
			System.out.println(textInsideInputLabelBox);
		}

		else {
			System.out.println(textInsideInputBox);
		}

		WebElement resume = wait.until(ExpectedConditions.visibilityOfElementLocated(resumeLocator));
		textInsideInputBox = resume.getAttribute("value");


		if (textInsideInputBox.isEmpty()) {
			WebElement resumelabel = wait.until(ExpectedConditions.visibilityOfElementLocated(resumeLabelLocator));
			textInsideInputLabelBox = resumelabel.getText();
			System.out.println(textInsideInputLabelBox);

		}

		else {
			System.out.println(textInsideInputBox);
		}
		WebElement radiobtn = wait.until(ExpectedConditions.visibilityOfElementLocated(radiobtnLocator));
		if (radiobtn.isSelected()) {

			System.out.println(radiobtn.getText());

			WebElement resumelabel = wait.until(ExpectedConditions.visibilityOfElementLocated(resumeLabelLocator));
			textInsideInputLabelBox = resumelabel.getText();
			System.out.println(textInsideInputLabelBox);

		}

		else {
			WebElement radiobtnlabel = wait.until(ExpectedConditions.visibilityOfElementLocated(radioLabelLocator));
			textInsideInputLabelBox = radiobtnlabel.getText();
			System.out.println(textInsideInputLabelBox);



		}

	}


	@Test(testName = "Verify DB data", description = " verifies DB data with actual data ", priority = 5)
	public void DBValidationTest() {

		WebElement firstname = wait.until(ExpectedConditions.visibilityOfElementLocated(firstNameLocator));
		firstname.sendKeys("Nidhi");
		String firstNameInsideInputBox = firstname.getAttribute("value");

		WebElement lastName = wait.until(ExpectedConditions.visibilityOfElementLocated(lastNameLocator));
		lastName.sendKeys("Chauhan");
		String lastNameInsideInputBox = lastName.getAttribute("value");


		WebElement email = wait.until(ExpectedConditions.visibilityOfElementLocated(emailLocator));
		email.sendKeys("nidhichauhan8898@gmail.com");
		String emailInsideInputBox = email.getAttribute("value");


		WebElement company = wait.until(ExpectedConditions.visibilityOfElementLocated(currentCompanyLocator));
		company.sendKeys("IBM");
		String companyInsideInputBox = company.getAttribute("value");


		WebElement mobile = wait.until(ExpectedConditions.visibilityOfElementLocated(mobileLocator));
		mobile.sendKeys("8851273955");
		String mobileInsideInputBox = mobile.getAttribute("value");


		WebElement dob = wait.until(ExpectedConditions.visibilityOfElementLocated(dobLocator));
		dob.sendKeys("1996-09-11");
		String dobInsideInputBox = dob.getAttribute("value");


		WebElement position = wait.until(ExpectedConditions.visibilityOfElementLocated(postionLocator));
		position.sendKeys("Test Engineer");
		String positionInsideInputBox = position.getAttribute("value");


		WebElement portfolio = wait.until(ExpectedConditions.visibilityOfElementLocated(portfolioLocator));
		portfolio.sendKeys("www.linkedin.com/in/nidhi-chauhan-3a26b813b/");
		String portfolioInsideInputBox = portfolio.getAttribute("value");


		WebElement salary = wait.until(ExpectedConditions.visibilityOfElementLocated(salaryLocator));
		salary.sendKeys("8 LPA");
		String salaryInsideInputBox = salary.getAttribute("value");


		WebElement resume = wait.until(ExpectedConditions.visibilityOfElementLocated(resumeLocator));
		resume.sendKeys("/Users/nidhichauhan/eclipse-workspace/Sourcefuse_Assignment/Data/51183.xlsx");

		String resumeInsideInputBox = resume.getAttribute("value");


		WebElement radiobtn = wait.until(ExpectedConditions.visibilityOfElementLocated(radiobtnLocator));
		radiobtn.click();


		WebElement submitbtn = wait.until(ExpectedConditions.visibilityOfElementLocated(submitbtnLocator));
		submitbtn.click();



		actuallist.add(firstNameInsideInputBox+"\n"+lastNameInsideInputBox+"\n"+emailInsideInputBox+"\n"+companyInsideInputBox+"\n"+mobileInsideInputBox+"\n"+dobInsideInputBox+"\n"+positionInsideInputBox+"\n"+portfolioInsideInputBox+"\n"+salaryInsideInputBox+"\n"+resumeInsideInputBox);

		try {
			Class.forName("com.mysql.jdbc.Driver");

			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Sourcefuse", "root", "selenium");
			Statement st =con.createStatement();

			ResultSet rs= st.executeQuery(" Select * from  FormData");

			while(rs.next())  

				expectedlist.add(rs.getString(1)+"\n"+rs.getString(2)+"\n"+rs.getString(3)+"\n"+rs.getString(4)+"\n"+rs.getString(5)+"\n"+rs.getString(13)+"\n"+rs.getString(14)+"\n"+rs.getString(6)+"\n"+rs.getString(7)+"\n"+rs.getString(8));  

			con.close(); 

		}
		catch(Exception e)
		{ 
			System.out.println(e);
		} 

		assertEquals(actuallist, expectedlist);



		driver.close();
	}


	@Test(testName = "Verify email sent", description = " verifies email entry from db ", priority = 6)
	public void emailSentTest() {

		WebElement firstname = wait.until(ExpectedConditions.visibilityOfElementLocated(firstNameLocator));
		firstname.sendKeys("Nidhi");

		WebElement lastName = wait.until(ExpectedConditions.visibilityOfElementLocated(lastNameLocator));
		lastName.sendKeys("Chauhan");


		WebElement email = wait.until(ExpectedConditions.visibilityOfElementLocated(emailLocator));
		email.sendKeys("nidhichauhan8898@gmail.com");


		WebElement company = wait.until(ExpectedConditions.visibilityOfElementLocated(currentCompanyLocator));
		company.sendKeys("IBM");


		WebElement mobile = wait.until(ExpectedConditions.visibilityOfElementLocated(mobileLocator));
		mobile.sendKeys("8851273955");


		WebElement dob = wait.until(ExpectedConditions.visibilityOfElementLocated(dobLocator));
		dob.sendKeys("1996-09-11");


		WebElement position = wait.until(ExpectedConditions.visibilityOfElementLocated(postionLocator));
		position.sendKeys("Test Engineer");


		WebElement portfolio = wait.until(ExpectedConditions.visibilityOfElementLocated(portfolioLocator));
		portfolio.sendKeys("www.linkedin.com/in/nidhi-chauhan-3a26b813b/");


		WebElement salary = wait.until(ExpectedConditions.visibilityOfElementLocated(salaryLocator));
		salary.sendKeys("8 LPA");


		WebElement resume = wait.until(ExpectedConditions.visibilityOfElementLocated(resumeLocator));
		resume.sendKeys("/Users/nidhichauhan/eclipse-workspace/Sourcefuse_Assignment/Data/51183.xlsx");


		WebElement radiobtn = wait.until(ExpectedConditions.visibilityOfElementLocated(radiobtnLocator));
		radiobtn.click();


		WebElement submitbtn = wait.until(ExpectedConditions.visibilityOfElementLocated(submitbtnLocator));
		submitbtn.click();


		try {
			Class.forName("com.mysql.jdbc.Driver");

			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Sourcefuse", "root", "selenium");
			Statement st =con.createStatement();

			ResultSet rs= st.executeQuery(" Select * from  FormData");
			while(rs.next())  
				a = rs.getBoolean(12) ;
			con.close(); 

		}
		catch(Exception e)
		{ 
			System.out.println(e);
				
		}
		
		
		assertEquals(a.booleanValue(), true);
		driver.close();
	} }



















































